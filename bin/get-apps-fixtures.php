<?php

require_once __DIR__.'/../vendor/autoload.php';

use Skript\Nextcloud\Packages\Api;

$api = new Api();
$releases = $api->getReleases();

foreach ($releases as $release) {
    echo "Fetching apps for ".$release['version']."\n";
    $apps = $api->getApps($release['version']);
    
    if (!$apps) {
        echo "Failed!\n";
        continue;
    }
    $path = __DIR__.'/../tests/Fixtures/api/v1/platform/'.$release['version'];
    
    @mkdir($path, 0775);
    
    file_put_contents(
        $path.'/apps.json',
        json_encode($apps)
    );
    
    echo "Written apps.json to disc\n";
    
    sleep(1);
}