# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.3.0] - 2023-10-13

### Fixed
- Prevented breaking entire update when some downloads fail (#2)
- Out of memory issues on satis build (#4)

### Changed
- Updated to PHP 8.2 (#5)
- Updated Satis to latest version

### Added
- Error logging

## [0.2.1] - 2019-11-01
### Fixed 
- Fixed php and platform version constraints (#1)

## [0.2.0] - 2019-10-25
### Changed
- Release index scraping is swapped with call to apps api to collect releases

### Added
- Requirements and extra info are added to app packages
- Added ETag based caching of api calls
- Apps are fetched for all platform versions

### Removed
- Release index scraping

## [0.1.0] - 2019-10-08
### Added
- Added gitlab ci pipeline for satis repository distribution
- Added Nextcloud apps to satis.json generation, fetched from Nextcloud's apps api
- Wrote script to generate a satis.json file from Nextcloud release index

[Unreleased]: https://gitlab.com/skript-cc/common/nextcloud-packages/compare/0.3.0...master
[0.3.0]: https://gitlab.com/skript-cc/common/nextcloud-packages/compare/0.2.1...0.3.0
[0.2.1]: https://gitlab.com/skript-cc/common/nextcloud-packages/compare/0.2.0...0.2.1
[0.2.0]: https://gitlab.com/skript-cc/common/nextcloud-packages/compare/0.1.0...0.2.0
[0.1.0]: https://gitlab.com/skript-cc/common/nextcloud-packages/-/tags/0.1.0