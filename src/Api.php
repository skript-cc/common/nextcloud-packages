<?php

declare(strict_types=1);

namespace Skript\Nextcloud\Packages;

use function Skript\Nextcloud\Packages\Log\log;

function parseResponseHeaders(array $rawHeaders): array
{
    $statusCode = -1;
    $headers = array_reduce(
        $rawHeaders,
        function ($carry, $header) use (&$statusCode)
        {
            if (!preg_match('/^([a-z\-]+):(.*)/i', $header, $nameValue)) {
                $carry[] = $header;
                // check for http status header
                if (preg_match("#HTTP/[0-9\.]+\s+([0-9]+)#", $header, $match)) {
                    $statusCode = intval($match[1]);
                }
            } else {
                // process regular headers
                [$header, $name, $value] = $nameValue;
                $carry[strtoupper($name)] = trim($value);
            }
            return $carry;
        },
        []
    );
    return [$statusCode, $headers];
}

class Api
{
    const DEFAULT_BASE_URL = 'https://apps.nextcloud.com/api';
    const DEFAULT_VERSION = 'v1';

    const RESPONSE_SUCCESS = 0;
    const RESPONSE_HEADERS = 1;
    const RESPONSE_BODY = 2;
    const RESPONSE_STATUSCODE = 3;

    protected $baseUrl;
    protected $version;

    public function __construct(
        string $baseUrl=self::DEFAULT_BASE_URL,
        string $version=self::DEFAULT_VERSION
    ) {
        $this->baseUrl = $baseUrl;
        $this->version = $version;
    }
    
    public function getBaseUrl(): string
    {
        return implode(
            '/', [
                rtrim($this->baseUrl, '/'),
                trim($this->version, '/')
            ]
        );
    }
    
    public function sendRequest(string $endpoint, string $method='GET'): array
    {
        $url = implode('/', [$this->getBaseUrl(), $endpoint]);
        $opts = [
            'http' => [
                'method' => $method,
                'header' => []
            ]
        ];
        
        // read cache
        $cachePath = __DIR__.'/../.cache';
        $cacheHash = md5($url.$method);
        $cacheFile = $cachePath.'/'.$cacheHash.'.php';
        
        if (file_exists($cacheFile)) {
            $cache = include $cacheFile;
            $opts['http']['header'][] = 'If-None-Match: '.$cache['etag'];
        }
        
        // make request
        try {
            log()->info("GET $url");
            $result = file_get_contents(
                $url,
                false,
                substr($url, 0, 4) === 'http'
                    ? stream_context_create($opts)
                    : null
            );
        } catch (\Exception $e) {
            $result = false;
        }
        
        // parse response
        [$resStatusCode, $resHeaders] = parseResponseHeaders(
            $http_response_header ?? []
        );
        log()->info("${resStatusCode}");
        
        if ($resStatusCode === 304) {
            // return data from cache
            $resultData = $cache['data'];
        } else {
            // return data from response
            $resultData = [
                self::RESPONSE_SUCCESS => $result !== false,
                self::RESPONSE_HEADERS => $resHeaders,
                self::RESPONSE_BODY => $result,
                self::RESPONSE_STATUSCODE => $resStatusCode
            ];
            
            // write cache
            if ($result && isset($resHeaders['ETAG'])) {
                file_put_contents(
                    $cacheFile,
                    '<?php return '
                    . var_export(
                        [
                            'etag' => $resHeaders['ETAG'],
                            'data' => $resultData
                        ],
                        true
                    )
                    . ';'
                );
            }
        }
        
        return $resultData;
    }
    
    public function getReleases(): ?array
    {
        [$succes, $headers, $body] = $this->sendRequest('platforms.json');
        return $succes ? json_decode($body, true) : null;
    }

    public function getApps(string $platformVersion): ?array
    {
        [$succes, $headers, $body] = $this->sendRequest("platform/{$platformVersion}/apps.json");
        return $succes ? json_decode($body, true) : null;
    }
}