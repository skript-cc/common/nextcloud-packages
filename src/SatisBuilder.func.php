<?php

declare(strict_types=1);

namespace Skript\Nextcloud\Packages\SatisBuilder;

function createSatisConfig(
    string $name, string $homepage=null, array $packages = []
): array {
    $config = [
        'name' => $name,
        'repositories' => [],
        'require-all' => true,
    ];
    if ($homepage) {
        $config['homepage'] = $homepage;
    }
    if (count($packages) > 0) {
        $config['repositories'][] = [
            'type' => 'package',
            'package' => $packages
        ];
    }
    return $config;
}

function writeSatisConfigAsJSON(array $satisConfig, $stream): int
{
    $bytesWritten = fwrite(
        $stream,
        json_encode($satisConfig, JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
    );
    return $bytesWritten !== false ? $bytesWritten : -1;
}