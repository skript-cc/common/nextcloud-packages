<?php

declare(strict_types=1);

namespace Skript\Nextcloud\Packages\GenerateCommand;

use Skript\Nextcloud\Packages\Api;
use function Skript\Nextcloud\Packages\Log\log;

use function Skript\Nextcloud\Packages\SatisBuilder\{
    createSatisConfig,
    writeSatisConfigAsJSON
};
use function Skript\Nextcloud\Packages\Collector\{
    collectServerReleasesFromApi,
    collectAppReleasesFromApi
};

const ARG_PROGRAM = 0;
const OPT_HELP = 0;
const OPT_URL = 1;
const OPT_NAME = 2;
const OPT_HOMEPAGE = 3;
const OPT_API_URL = 4;
const OPT_SILENT = 5;

function usage(string $program): string
{
    return <<<HELP
Generates a satis.json file from the nextcloud release index

Usage:
  $program [options]

Options:
  -h, --help    Shows this help page
  --url         Url to the nextcloud release index
  --api-url     Url to the nextcloud (apps) api
  --name        Name to use in the satis config
  --homepage    Homepage url to use in the satis config
  --silent      Flag to disable info and warning output
HELP;
}

function parseArguments(array $args): array
{
    // split opts with = assignment into two parts
    $cleanArgs = [];
    foreach ($args as $arg) {
        $optMatches = [];
        if (preg_match('/(\-+[^=]+)=(.*)/i', $arg, $optMatches)) {
            $cleanArgs[] = $optMatches[1];
            $cleanArgs[] = $optMatches[2];
        } else {
            $cleanArgs[] = $arg;
        }
    }
    $args = $cleanArgs;
    
    return [
        [
            ARG_PROGRAM => $args[0] ?? 'generate-satis-json'
        ],
        [
            OPT_HELP => in_array('-h', $args) || in_array('--help', $args),
            OPT_URL => ($urlIndex=array_search('--url', $args)) !== false
                ? $args[$urlIndex+1]
                : 'https://download.nextcloud.com/server/releases/',
            OPT_NAME => ($nameIndex=array_search('--name', $args)) !== false
                ? $args[$nameIndex+1]
                : 'skript.cc/nextcloud-packages',
            OPT_HOMEPAGE => ($homepageIndex=array_search('--homepage', $args)) !== false
                ? $args[$homepageIndex+1]
                : null,
            OPT_API_URL => ($apiIndex=array_search('--api-url', $args)) !== false
                ? $args[$apiIndex+1]
                : null,
            OPT_SILENT => in_array('--silent', $args)
        ]
    ];
}

function run(array $argv = [], $stream=STDOUT): void
{
    [$args, $opts] = parseArguments($argv);
    
    if ($opts[OPT_HELP]) {
        echo usage($args[ARG_PROGRAM]);
        return;
    }
    
    if ($opts[OPT_SILENT]) {
        error_reporting(E_ERROR);
    }
    
    $api = $opts[OPT_API_URL]
        ? new Api($opts[OPT_API_URL])
        : new Api();
    
    $config = createSatisConfig(
        $opts[OPT_NAME],
        $opts[OPT_HOMEPAGE],
        array_merge(
            collectServerReleasesFromApi($api),
            collectAppReleasesFromApi($api)
        )
    );
    writeSatisConfigAsJSON($config, $stream);
    
    $memUsed = memory_get_peak_usage(true);
    log()->info("Peak memory usage: {$memUsed} bytes");
}