<?php

declare(strict_types=1);

namespace Skript\Nextcloud\Packages\Collector;

use Skript\Nextcloud\Packages\Api;
use Composer\Semver\VersionParser;

use function Skript\Nextcloud\Packages\Log\log;

function collectServerReleasesFromApi(Api $api): array
{
    return array_map(
        function($releaseInfo) {
            $version = $releaseInfo['version'];
            $url = "https://download.nextcloud.com/server/releases/nextcloud-{$version}.zip";
            return [
                'name' => 'nextcloud/server',
                'type' => 'nextcloud-server',
                'version' => $version,
                'dist' => [
                    'url' => $url,
                    'type' => 'zip'
                ]
            ];
        },
        $api->getReleases()
    );
}

function collectAppReleasesFromApi(Api $api): array
{
    $releases = $api->getReleases();
    $packages = [];
    foreach ($releases as $release) {
        if (!$release['hasRelease']) {
            continue;
        }
        
        $apps = $api->getApps($release['version']);
        if (!$apps) {
            // optionally save this failure somewhere to a list of failed
            // downloads that can be retried later
            log()->warning(
                "Failed to fetch apps for nextcloud release "
                . $release['version']
            );
            continue;
        }
        
        foreach ($apps as $app) {
            $packages = array_merge($packages, mapAppToPackages($app));
        }
    }
    return array_values($packages);
}

function mapAppToPackages(array $app): array
{
    $name = 'nextcloud/'.$app['id'];
    $keys = [];
    $packages = array_map(
        function ($release) use ($name, &$keys, $app) {
            $version = $release['version'];
            $normalizedVersion = normalizeVersion($version);
            if ($version !== $normalizedVersion) {
                log()->notice(
                    "{$name} version is normalized from {$version} to"
                    . " {$normalizedVersion}"
                );
            }
            $keys[] = $name.'@'.$normalizedVersion;
            
            return array_filter([
                'name' => $name,
                'type' => 'nextcloud-app',
                'version' => $normalizedVersion,
                'dist' => [
                    'url' => $release['download'],
                    'type' => getTypeFromUrl($release['download'])
                ],
                'require' => array_merge(
                    [
                        'nextcloud/server' => $release['platformVersionSpec'],
                        'php' => $release['phpVersionSpec'],
                    ],
                    array_reduce(
                        $release['phpExtensions'],
                        function(array $carry, array $phpExt) {
                            $carry['phpext-'.$phpExt['id']] = $phpExt['versionSpec'];
                            return $carry;
                        },
                        []
                    )
                ),
                // some other niceties
                'description' => $app['translations']['en']['summary'] ?? null,
                'keywords' => $app['categories'] ?? null,
                'homepage' => 'https://apps.nextcloud.com/apps/'.$app['id'],
                'support' => [
                    'issues' => $app['issueTracker'] ?? null
                ],
                'authors' => isset($app['authors'])
                    ? array_map(
                        function($author) {
                            return array_filter([
                                'name' => $author['name'] ?? null,
                                'email' => $author['mail'] ?? null,
                                'homepage' => $author['homepage'] ?? null
                            ]);
                        },
                        $app['authors']
                    )
                    : null
            ]);
        },
        $app['releases']
    );
    return array_combine($keys, $packages);
}

function getTypeFromUrl(string $url): string
{
    return preg_match('/\.tar(\.gz)*$/i', $url) ? 'tar' : 'zip';
}

/**
 * Composer is a bit strict on the formatting of version strings and doesn't 
 * allow something used in the nextcloud passwords app: 2019.9.1-build3180. The
 * part after the dash isn't an allowed prerelease version. Allowed values are: 
 * stable|beta|b|RC|alpha|a|patch|pl|p + numerical modifiers.
 */
function normalizeVersion(string $version): ?string
{
    $parser = new VersionParser();
    try {
        $parser->normalize($version);
    } catch (\UnexpectedValueException $e) {
        // try to figure out what's wrong
        $semverMatches = [];
        preg_match(
            '/([0-9]+)\.([0-9]+)\.([0-9]+)-?([0-9A-Za-z\.\-]*)\+?([0-9A-Za-z\.\-]*)/i',
            $version,
            $semverMatches
        );
        @list($fullVersion, $major, $minor, $patch, $pre, $build) = $semverMatches;
        
        if ($major === null || $minor === null || $patch === null) {
            throw new \UnexpectedValueException("Invalid version format {$version}");
        }
        
        // check pre values
        if ($pre) {
            $alphaPreMatches = [];
            preg_match('/^[a-z]+/i', $pre, $alphaPreMatches);
            @list($alphaPrefix) = $alphaPreMatches;
            
            $allowedComposerPreVersion = in_array(
                $alphaPrefix,
                ['stable','beta','b','RC','alpha','a','patch','pl','p']
            );
            
            if (!$allowedComposerPreVersion) {
                $build = (!empty($build) ? $build.'.' : '') . $pre;
                $pre = '';
            }
        }
        
        // put all parts back together
        return "{$major}.{$minor}.{$patch}"
            .(!empty($pre) ? "-{$pre}" : '')
            .(!empty($build) ? "+{$build}" : '');
    }
    return $version;
}