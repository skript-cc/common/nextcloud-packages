<?php

declare(strict_types=1);

namespace Skript\Nextcloud\Packages\Log;

use Monolog\Level;
use Monolog\Logger;
use Monolog\Handler\ErrorLogHandler;

function levelFromString(string $levelName) {
  switch ($levelName) {
    case 'NONE':
      return null;
    case 'DEBUG':
      return Level::Debug;
    case 'INFO':
      return Level::Info;
    case 'NOTICE':
      return Level::Notice;
    case 'WARNING':
      return Level::Warning;
    case 'ERROR':
      return Level::Error;
    case 'CRITICAL':
      return Level::Critical;
    case 'ALERT':
      return Level::Alert;
    default:
      throw new Error('Invalid log level');
  }
}

function logger(string $name): Logger {
  $logger = new Logger($name);
  $logLevel = levelFromString(getenv('LOGLEVEL') ?: 'WARNING');
  if ($logLevel !== null) {
    $handler = new ErrorLogHandler(
      ErrorLogHandler::OPERATING_SYSTEM,
      $logLevel
    );
    $logger->pushHandler($handler);
  }
  return $logger;
}

class MainLog {
  protected static $inst;
  public static function get() {
    if (!self::$inst) self::$inst = logger('Main');
    return self::$inst;
  }
}

function log() {
  return MainLog::get();
}