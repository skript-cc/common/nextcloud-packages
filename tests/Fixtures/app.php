<?php

return array (
  'id' => 'files_rightclick',
  'categories' => 
  array (
    0 => 'files',
    1 => 'tools',
  ),
  'userDocs' => '',
  'adminDocs' => '',
  'developerDocs' => '',
  'issueTracker' => 'https://github.com/nextcloud/files_rightclick/issues',
  'website' => 'https://github.com/nextcloud/files_rightclick',
  'created' => '2018-02-05T20:36:27.795309Z',
  'lastModified' => '2019-08-21T13:04:48.031751Z',
  'releases' => 
  array (
    0 => 
    array (
      'version' => '0.15.1',
      'phpExtensions' => 
      array (
      ),
      'databases' => 
      array (
      ),
      'shellCommands' => 
      array (
      ),
      'phpVersionSpec' => '*',
      'platformVersionSpec' => '>=14.0.0 <18.0.0',
      'minIntSize' => 32,
      'download' => 'https://github.com/nextcloud/files_rightclick/releases/download/v0.15.1/files_rightclick.tar.gz',
      'created' => '2019-08-21T13:04:48.022343Z',
      'licenses' => 
      array (
        0 => 'agpl',
      ),
      'lastModified' => '2019-08-21T13:04:48.109135Z',
      'isNightly' => false,
      'rawPhpVersionSpec' => '*',
      'rawPlatformVersionSpec' => '>=14 <=17',
      'signature' => 'fcl8Wo60+HfRASDc3j52qSA/3aH8KWizlP4FVc/NHdmWZ5HPRNAEhy/BIJXW3C+p
A0T+PozXifAsLTXVtUXiZnVLKFJ8MBzaNOiMRkzg4T5uCtd+RbJOUYXvfuj7Iddw
3JPzhXGvtg9Yez1wlYR1ihu6TRVMDfiQDpglrT65SmjvhScEBh8/swFu8m5nLnkD
YkO5jSQkhvoQ+aDDeBAjSI4ZaMh9BiULSoThnCAFEN0mHiGD8anQ2859a/WPnq0j
pl+0fs0Ac1uoP8kQlz3l+J8tp9moyG4LIpLcSYNY+yEAk+XjMnZGomjvDZRVK8Lv
QG2ZtpY0FGnysr4zb+EOLqrFGUF0rBaJaFQT6kaoSX9OqN0+ksmenR8dHrOoHNyU
8Zu6Wl+XD1TZAyVmKCwjplZvOOga6DlP35uN21uYVSKuKTzTgzDZ8VB/zE2wowMw
hY9OA64/PHuISApjNZjfedxEo6qzmLFY9n1U6b5vYL33cBY8yOcM0/ujSnH7VUZm
rc2LIXs8I3Eqofrv4NUQDl9vEnRW8Q2lmSiMAsIlQm++3tk4BdNoG1++8Oq9pctW
PSXFi3/wRVjjFYU0Xlkki1qrJud/sOQ0l0D5iKWWAJKVQHgkvr1edTYy7DoQfuI8
XZDiRgBWO+iI8JJDD041pIkVfCNfqti9rPzJmb4yYOc=',
      'translations' => 
      array (
        'en' => 
        array (
          'changelog' => '- The right click menu was not loaded.',
        ),
      ),
      'signatureDigest' => 'sha512',
    ),
    1 => 
    array (
      'version' => '0.15.0',
      'phpExtensions' => 
      array (
      ),
      'databases' => 
      array (
      ),
      'shellCommands' => 
      array (
      ),
      'phpVersionSpec' => '*',
      'platformVersionSpec' => '>=14.0.0 <18.0.0',
      'minIntSize' => 32,
      'download' => 'https://github.com/nextcloud/files_rightclick/releases/download/v0.15.0/files_rightclick.tar.gz',
      'created' => '2019-08-20T23:20:00.712574Z',
      'licenses' => 
      array (
        0 => 'agpl',
      ),
      'lastModified' => '2019-08-20T23:20:00.818334Z',
      'isNightly' => false,
      'rawPhpVersionSpec' => '*',
      'rawPlatformVersionSpec' => '>=14 <=17',
      'signature' => 'vsAKHjSZfVvCRpIDWdyezT1zEMqrZ8N7znm3DF8d0qgAgJRaIbKUABfQ8laMVsoI
eclbQeHy9PTj3yIVeXQYbeqq5E9p/PlckotpYSHQm/AmucH1/xxFEdQ4NgPk6ObV
gt9asUJ+BfIKqaeFKuXK8amJx9j3DEgX3M0iE9HIfXiXTrGNz3+XNnvOpd/FpMiA
BiE1sAR4kmlbxje5hX1e3D25iiv1TKy8L5CTyNZXUPXsAFzy+DsL53DhQ9I4wCRs
ismyiofguYpMLsoZZlxRubRk95c1u9/ANQstlYeapcBsFWalR0Te9KD0rxOf44F8
Q146XQgwcLzbXvtJmHv6W1fUWhq7LjoifbACxD379zLGy2hmPgBj1esKeBwqAEjG
BME47xZwUgxqO6DmPO4Pn05Z8Xensm7HgM2SktZwpnRMn/exRm2OoFK03ShDhjlx
bGyNXx7ENhPKlOe5APRIqBhxkinvCEtx+B8DUSPim+40i+2A2tfXPES6kUZMkCGE
2+8uPXg9W9P9ZAdPj+9YtfWmbyVstAeXkwhi86Ss1XK8H8PjzPj1BQK2bLs2Nc7a
NP8yxeXR0wVitI1+xJ6Xsf67jtQMHnQKJ+vq4yZIZmxKriPN2IcB8OeIPLRBxroh
sRXqK9WG4QXwb5ptWJ0i0csxNRR+xZUv41aqSsknea8=',
      'translations' => 
      array (
        'en' => 
        array (
          'changelog' => '- Add multiple translations thanks to Transifex
- RightClick does no more load in log in screen.
- Fix IE11 incompatibily',
        ),
      ),
      'signatureDigest' => 'sha512',
    ),
    2 => 
    array (
      'version' => '0.13.0',
      'phpExtensions' => 
      array (
      ),
      'databases' => 
      array (
      ),
      'shellCommands' => 
      array (
      ),
      'phpVersionSpec' => '*',
      'platformVersionSpec' => '>=10.0.0 <18.0.0',
      'minIntSize' => 32,
      'download' => 'https://github.com/nextcloud/files_rightclick/releases/download/v0.13.0/files_rightclick.tar.gz',
      'created' => '2019-03-28T11:43:25.522963Z',
      'licenses' => 
      array (
        0 => 'agpl',
      ),
      'lastModified' => '2019-03-28T11:43:28.214598Z',
      'isNightly' => false,
      'rawPhpVersionSpec' => '*',
      'rawPlatformVersionSpec' => '>=10 <=17',
      'signature' => 'Wi9AezCAqVMs5TYOPc1siciJPw48w8IzlRsccnzE1ISGqsi5aOwh0B1bacdH5Wtf
tpxWbDcLhpKJrjVvfp4wkDn1+IqCIxWDmdaCOo8+y/djk82PzDE+QhF3JuKw7Ssl
1uKzbToduepengmueYmlwH0hybOSo1eLASt69hpFwCj4xvYNGOZnrgMUHLIHNbYo
D4nn1niu2nsRtwxI1ygQsRXvUesURPLACbpDLV5m8biqclWoKMc/drQaSf37b/NV
tlZb9D4G4zvVZiu0oGlW1U5l3/3SsC7hga4XkdfXAKiASMiozejVXYAdsAChxFds
4WWRUDeDoIvarVWcotTzztTaC5eabbDCeYwzz86U4IWV5hcgfw5kVf7a2jReo7c0
qJ4xIKUDxZF4uFSrZlIrD+njLeJ4/sZ4HXJyJ9wFl4egltIE/VmJviKnCoLmCw45
aggehPh9EDQ7DvLMlk3Ts+PXnSGRpyZnLj0vE5FVP3Jco0BdiSUYJJqCbjVoqy0+
XwBX7zpLk5HSgCScVRpddNGJ0WgqOBZb7oIJ/HVHJT295wQNCup4PrhanUen7xEo
3ic95i9bOwQBoIxZuSNFgqytQhM/IAhX446WsJ9ZwOl2NiGD5+IUKE107Mk9rXVt
7zngwY5+KHT2dvlwHezasQBj3JzXeVF+p51wzXzUwxY=',
      'translations' => 
      array (
        'en' => 
        array (
          'changelog' => '- Remove regular click option: "Open folder" for example
- Improve outside clicks detection
- Code cleaning',
        ),
      ),
      'signatureDigest' => 'sha512',
    ),
  ),
  'screenshots' => 
  array (
    0 => 
    array (
      'url' => 'https://raw.githubusercontent.com/nextcloud/files_rightclick/master/screenshots/picture.png',
      'smallThumbnail' => '',
    ),
  ),
  'translations' => 
  array (
    'en' => 
    array (
      'name' => 'Right click',
      'summary' => 'Right click menu for Nextcloud',
      'description' => 'This app allows users and developers to have a right click menu. Simply use the RightClick object to quickly create context menus. The Files app already shows the actions menu when right clicking on files and folders.',
    ),
  ),
  'isFeatured' => false,
  'authors' => 
  array (
    0 => 
    array (
      'name' => 'NASTUZZI Samy',
      'mail' => 'samy@nastuzzi.fr',
      'homepage' => 'https://samy.nastuzzi.fr',
    ),
  ),
  'ratingRecent' => 0.5,
  'ratingOverall' => 0.928571428571429,
  'ratingNumRecent' => 0,
  'ratingNumOverall' => 7,
  'certificate' => '-----BEGIN CERTIFICATE-----
MIIECzCCAvMCAhCRMA0GCSqGSIb3DQEBCwUAMHsxCzAJBgNVBAYTAkRFMRswGQYD
VQQIDBJCYWRlbi1XdWVydHRlbWJlcmcxFzAVBgNVBAoMDk5leHRjbG91ZCBHbWJI
MTYwNAYDVQQDDC1OZXh0Y2xvdWQgQ29kZSBTaWduaW5nIEludGVybWVkaWF0ZSBB
dXRob3JpdHkwHhcNMTgwMjA5MDg0MDU4WhcNMjgwNTE3MDg0MDU4WjAbMRkwFwYD
VQQDDBBmaWxlc19yaWdodGNsaWNrMIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIIC
CgKCAgEAw4O0p3Yuqixh4Qb7LjFiDWh1AUNkNDCpCmxQEaU/BBACEON+EO/s5Tig
0fbuOpuwlMuz20bgBYTNK+yQJ+hDGbzLjUc5A8PefRgn6nmd9SdxPbcWL+DfqF6Y
okXr3qUQ2uBzRgrTvnX0TI6vD3l9OPoPJ76Gm76/wuuzvlS9TqOrFEHB+aR2gdTk
xWwz+vh1ZrF9BC7RFoXePs8jnRmj28MozDXlP3JqJ0uZHKIcGeeZiW67CPO9n2vw
T4lEVEhscVDD642x8GAS9owdxMHjaa4tDAWuA3lB0AhLx6XWaZNI9s/aX8f9NuLv
7J8sLdXkwWw9lJxbB+JS2dWPKRAfxt1Uc4oqhnbU8SB1ivHEjL0louulNsO/ML7i
crjdIYNk4w0gS7aWgJwHLhHDielLprU6niytQtIzWk/7M8Xb4GoL0IczxVM08KS7
D+m2jtfEx+wuegCiV+pTY4LfDTQI9QVWnDR2/xlXUqHPIBHoiTY4L27JknAaeh8f
5aVKXTDWx09XR6ZG+E65OQ7xgo1B8dPFLTWK/WxeXjoG8caA6Ok0RQfPtBx7GhJS
gD8s4Tqb890ss8+o5AfSQtqq6tPO44/kPF0tNEo3woUMEEoCmroOhZFoYVQzjCo9
74oyKf3RBWQjJd/n6TpXATS0veEoR8ENmLxpT9FHdzXycjemKAECAwEAATANBgkq
hkiG9w0BAQsFAAOCAQEAk2CtiikkSHQ/RAEeijBIXSNUksSH4Gl1gBdhO4wpNLnL
QEbJ5z7Gc+y0ZcqNpwx7zjd2gEC4gKx4ugV69EwQ6tZATGHiD9Qm5qW1VsZSOR2f
b7q1P7rgY31p3aT/ngxoPF70W3nkQn7osT/pTFRpQY0ZGtljSHrKCdZisvnzj7Sy
8ZuTe/LCXa40USZofmv7ClZfPeL/XBEu/GtZdRoWVOy5/xJnE9tMraJSjbEmFGCK
WG58zONC0AZO4+Hrt4+re/iR+nNWf33HWUp1eiYjQdpz3XTzW1I7G+Gd5xONwAgA
lp3+w4q5nC2ItuVLf2DP3dF5JW3ZAH8p3TaV38ZXRw==
-----END CERTIFICATE-----',
  'discussion' => 'https://github.com/nextcloud/files_rightclick',
);