<?php

declare(strict_types=1);

namespace Skript\Nextcloud\Packages\Collector;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Error\Notice;
use Skript\Nextcloud\Packages\Api;

function parseSemver(string $semver)
{
    $versionMatches = [];
    if (!preg_match('/^(\d+)\.(\d+)\.(\d+)/', $semver, $versionMatches)) {
        throw new ErrorException('Invalid semantic version given');
    }
    return [
        (int) $versionMatches[1],
        (int) $versionMatches[2],
        (int) $versionMatches[3],
    ];
}

final class CollectorTest extends TestCase
{
    protected $api;
    
    protected function getApp()
    {
        return include __DIR__.'/Fixtures/app.php';
    }
    
    public function setup(): void
    {
        $this->api = new Api(__DIR__.'/Fixtures/api');
    }
    
    public function testMapAppToPackagesParsesEachAppReleaseAsPackage()
    {
        $this->assertEquals(
            [
                'nextcloud/files_rightclick@0.15.1' => [
                    'name' => 'nextcloud/files_rightclick',
                    'type' => 'nextcloud-app',
                    'version' => '0.15.1',
                    'dist' => [
                        'url' => 'https://github.com/nextcloud/files_rightclick/releases/download/v0.15.1/files_rightclick.tar.gz',
                        'type' => 'tar'
                    ],
                    'require' => [
                        'nextcloud/server' => '>=14.0.0 <18.0.0',
                        'php' => '*'
                    ],
                    'description' => 'Right click menu for Nextcloud',
                    'keywords' => ['files', 'tools'],
                    'homepage' => 'https://apps.nextcloud.com/apps/files_rightclick',
                    'support' => ['issues' => 'https://github.com/nextcloud/files_rightclick/issues'],
                    'authors' => [
                        [
                            'name' => 'NASTUZZI Samy',
                            'email' => 'samy@nastuzzi.fr',
                            'homepage' => 'https://samy.nastuzzi.fr',
                        ]
                    ]
                ],
                'nextcloud/files_rightclick@0.15.0' => [
                    'name' => 'nextcloud/files_rightclick',
                    'type' => 'nextcloud-app',
                    'version' => '0.15.0',
                    'dist' => [
                        'url' => 'https://github.com/nextcloud/files_rightclick/releases/download/v0.15.0/files_rightclick.tar.gz',
                        'type' => 'tar'
                    ],
                    'require' => [
                        'nextcloud/server' => '>=14.0.0 <18.0.0',
                        'php' => '*'
                    ],
                    'description' => 'Right click menu for Nextcloud',
                    'keywords' => ['files', 'tools'],
                    'homepage' => 'https://apps.nextcloud.com/apps/files_rightclick',
                    'support' => ['issues' => 'https://github.com/nextcloud/files_rightclick/issues'],
                    'authors' => [
                        [
                            'name' => 'NASTUZZI Samy',
                            'email' => 'samy@nastuzzi.fr',
                            'homepage' => 'https://samy.nastuzzi.fr',
                        ]
                    ]
                ],
                'nextcloud/files_rightclick@0.13.0' => [
                    'name' => 'nextcloud/files_rightclick',
                    'type' => 'nextcloud-app',
                    'version' => '0.13.0',
                    'dist' => [
                        'url' => 'https://github.com/nextcloud/files_rightclick/releases/download/v0.13.0/files_rightclick.tar.gz',
                        'type' => 'tar'
                    ],
                    'require' => [
                        'nextcloud/server' => '>=10.0.0 <18.0.0',
                        'php' => '*'
                    ],
                    'description' => 'Right click menu for Nextcloud',
                    'keywords' => ['files', 'tools'],
                    'homepage' => 'https://apps.nextcloud.com/apps/files_rightclick',
                    'support' => ['issues' => 'https://github.com/nextcloud/files_rightclick/issues'],
                    'authors' => [
                        [
                            'name' => 'NASTUZZI Samy',
                            'email' => 'samy@nastuzzi.fr',
                            'homepage' => 'https://samy.nastuzzi.fr',
                        ]
                    ]
                ],
            ],
            mapAppToPackages($this->getApp())
        );
    }
    
    public function testCollectAppReleasesFromApiReturnsEachAppReleaseAsPackage()
    {
        // expect notice from version normalization
        // $this->expectException(Notice::class);
        
        $appPackages = collectAppReleasesFromApi($this->api);
        $this->assertCount(2059, $appPackages);
        
        $this->assertEquals(
            [
                'name' => 'nextcloud/files_rightclick',
                'type' => 'nextcloud-app',
                'version' => '0.15.1',
                'dist' => [
                    'url' => 'https://github.com/nextcloud/files_rightclick/releases/download/v0.15.1/files_rightclick.tar.gz',
                    'type' => 'tar'
                ],
                'require' => [
                    'nextcloud/server' => '>=14.0.0 <18.0.0',
                    'php' => '*'
                ],
                'description' => 'Right click menu for Nextcloud',
                'keywords' => ['files', 'tools'],
                'homepage' => 'https://apps.nextcloud.com/apps/files_rightclick',
                'support' => ['issues' => 'https://github.com/nextcloud/files_rightclick/issues'],
                'authors' => [
                    [
                        'name' => 'NASTUZZI Samy',
                        'email' => 'samy@nastuzzi.fr',
                        'homepage' => 'https://samy.nastuzzi.fr',
                    ]
                ]
            ],
            $appPackages[0]
        );
        
        $this->assertEquals(
            [
                "name" => "nextcloud/gpxpod",
                "type" => "nextcloud-app",
                "version" => "1.0.9",
                "dist" => [
                    "url" => "https://gitlab.com/eneiluj/gpxpod-oc/uploads/ac117daa67045db9c603e94d8150276c/gpxpod-1.0.9.tar.gz",
                    "type" => "tar"
                ],
                'require' => [
                    'nextcloud/server' => '>=9.0.0 <12.0.0',
                    'php' => '>=5.6.0'
                ],
                'description' => 'Display, analyse, compare and share GPS track files',
                'keywords' => ["multimedia", "social", "tools"],
                'homepage' => 'https://apps.nextcloud.com/apps/gpxpod',
                'support' => ['issues' => 'https://gitlab.com/eneiluj/gpxpod-oc/issues'],
                'authors' => [
                    ['name' => 'Julien Veyssier (@eneiluj)']
                ]
            ],
            end($appPackages)
        );
    }
    
    public function testCollectServerReleasesFromApiReturnsEachReleaseAsPackage()
    {
        $packages = collectServerReleasesFromApi($this->api);
        usort(
            $packages,
            function(array $a, array $b): int
            {
                [$aMajor, $aMinor, $aPatch] = parseSemver($a['version']);
                [$bMajor, $bMinor, $bPatch] = parseSemver($b['version']);
                
                if ($aMajor > $bMajor) return 1;
                if ($aMajor < $bMajor) return -1;
                if ($aMinor > $bMinor) return 1;
                if ($aMinor < $bMinor) return -1;
                if ($aPatch > $bPatch) return 1;
                if ($aPatch < $bPatch) return -1;
                
                return 0;
            }
        );
        
        $this->assertCount(80, $packages);
        $this->assertEquals(
            [
                'name' => 'nextcloud/server',
                'type' => 'nextcloud-server',
                'version' => '9.0.50',
                'dist' => [
                    'url' => 'https://download.nextcloud.com/server/releases/nextcloud-9.0.50.zip',
                    'type' => 'zip'
                ]
            ],
            $packages[0]
        );
        $this->assertEquals(
            [
                'name' => 'nextcloud/server',
                'type' => 'nextcloud-server',
                'version' => '17.0.0',
                'dist' => [
                    'url' => 'https://download.nextcloud.com/server/releases/nextcloud-17.0.0.zip',
                    'type' => 'zip'
                ]
            ],
            $packages[count($packages)-1]
        );
    }
    
    function testNormalizeVersionConvertsUnsupportedComposerVersions()
    {
        $this->assertEquals(
            '2019.9.1+build3180',
            normalizeVersion('2019.9.1-build3180')
        );
        
        $this->assertEquals(
            '2019.10.0+build3186',
            normalizeVersion('2019.10.0-build3186')
        );
    }
    
    function testGetTypeFromUrl()
    {
        $this->assertEquals(
            'tar',
            getTypeFromUrl('https://domain.com/download.tar.gz')
        );
        $this->assertEquals(
            'tar',
            getTypeFromUrl('https://domain.com/download.tar')
        );
        $this->assertEquals(
            'zip',
            getTypeFromUrl('https://domain.com/download.zip')
        );
    }
}