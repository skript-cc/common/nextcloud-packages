<?php

declare(strict_types=1);

namespace Skript\Nextcloud\Packages;

use PHPUnit\Framework\TestCase;

final class ApiTest extends TestCase
{
    public function getMockApi(): Api
    {
        return new Api(__DIR__.'/Fixtures/api');
    }
    
    public function testGetBaseUrlReturnsDefaultURL()
    {
        $this->assertEquals(
            'https://apps.nextcloud.com/api/v1',
            (new Api())->getBaseUrl()
        );
    }
    
    public function testGetBaseUrlReturnsCustomURL()
    {
        $customUrl = __DIR__.'/Fixtures/api';
        $this->assertEquals(
            $customUrl.'/'.Api::DEFAULT_VERSION,
            (new Api($customUrl))->getBaseUrl()
        );
    }
    
    public function testGetBaseUrlReturnsCustomVersion()
    {
        $this->assertEquals(
            Api::DEFAULT_BASE_URL.'/v2',
            (new Api(Api::DEFAULT_BASE_URL, 'v2'))->getBaseUrl()
        );
    }
    
    public function testSendRequestReturnsSuccessResponse()
    {
        $api = $this->getMockApi();
        [$succes, $headers, $body] = $api->sendRequest('platforms.json');
        
        $this->assertTrue($succes);
        $this->assertJSONStringEqualsJSONFile(
            $api->getBaseUrl().'/platforms.json',
            $body
        );
    }
    
    public function testGetReleasesReturnsParsedReleases()
    {
        $api = $this->getMockApi();
        
        $this->assertJSONStringEqualsJSONFile(
            $api->getBaseUrl().'/platforms.json',
            json_encode($api->getReleases())
        );
    }
    
    public function testGetAppsReturnsParsedApps()
    {
        $api = $this->getMockApi();
        $platformVersion = '17.0.0';
        
        $this->assertEquals(
            json_decode(
                file_get_contents(
                    $api->getBaseUrl()."/platform/$platformVersion/apps.json"
                ),
                true
            ),
            $api->getApps($platformVersion)
        );
    }
}