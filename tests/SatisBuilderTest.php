<?php 

declare(strict_types=1);

namespace Skript\Nextcloud\Packages\SatisBuilder;

use PHPUnit\Framework\TestCase;

final class SatisBuilderTest extends TestCase
{
    public function testCreateSatisConfigReturnsGivenName()
    {
        $name = 'Nextcloud composer repository';
        $config = createSatisConfig($name);
        $this->assertEquals($name, $config['name']);
    }
    
    public function testCreateSatisConfigReturnsGivenHomepage()
    {
        $name = 'Nextcloud composer repository';
        $homepage = 'https://nextcloud-packages.skript.cc';
        $config = createSatisConfig($name, $homepage);
        $this->assertEquals($homepage, $config['homepage']);
    }
    
    public function testCreateSatisConfigLeavesOutHomepage()
    {
        $name = 'Nextcloud composer repository';
        $config = createSatisConfig($name);
        $this->assertArrayNotHasKey('homepage', $config);
    }
    
    public function testCreateSatisConfigReturnsGivenPackages()
    {
        $name = 'Nextcloud composer repository';
        $packages = ['package1', 'package2'];
        $config = createSatisConfig($name, null, $packages);
        $this->assertEquals($packages, $config['repositories'][0]['package']);
    }
    
    public function testCreateSatisConfigLeavesOutPackages()
    {
        $name = 'Nextcloud composer repository';
        $config = createSatisConfig($name);
        
        $foundPackageRepos = array_filter(
            $config['repositories'],
            function ($repo) {
                return isset($repo['type']) && $repo['type'] === 'package';
            }
        );
        
        $this->assertLessThan(1, count($foundPackageRepos));
    }
    
    public function testWriteSatisConfigAsJSONWritesExpectedJSON()
    {
        $name = 'Nextcloud composer repository';
        $packages = ['package1', 'package2'];
        $homepage = 'https://nextcloud-packages.skript.cc';
        $stream = fopen('php://memory', 'rw+');
        
        $bytesWritten = writeSatisConfigAsJSON(
            createSatisConfig($name, $homepage, $packages),
            $stream
        );
        
        rewind($stream);
        $jsonString = stream_get_contents($stream);
        
        $this->assertGreaterThan(
            -1,
            $bytesWritten,
            'Function should not fail to write data'
        );
        
        $this->assertJSONStringEqualsJSONString(
            '{
              "name": "'.$name.'",
              "homepage": "'.$homepage.'",
              "repositories": [
                {
                    "type": "package",
                    "package": '.json_encode($packages).'
                }
              ],
              "require-all": true
            }',
            $jsonString
        );
        
        fclose($stream);
    }
}