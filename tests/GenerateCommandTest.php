<?php 

declare(strict_types=1);

namespace Skript\Nextcloud\Packages\GenerateCommand;

use PHPUnit\Framework\TestCase;
use PHPUnit\Framework\Error\Notice;

final class GenerateCommandTest extends TestCase
{
    public function testUsage()
    {
        $this->assertIsString(usage('program'));
    }
    
    public function testHelpOptionShowsUsage()
    {
        // test short opt
        ob_start();
        run(['program', '-h']);
        $output = ob_get_clean();
        
        $this->assertEquals(usage('program'), $output);
        
        // test long opt
        ob_start();
        run(['program', '--help']);
        $output = ob_get_clean();
        
        $this->assertEquals(usage('program'), $output);
    }
    
    public function testParseArgumentsSetsProgramName()
    {
        $argv = ['program-name', '--option'];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals('program-name', $args[ARG_PROGRAM]);
    }
    
    public function testParseArgumentsSetsHelpOptionToTrue()
    {
        // test long opt
        $argv = ['program-name', '--help'];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertTrue($opts[OPT_HELP]);
        
        // test short opt
        $argv = ['program-name', '-h'];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertTrue($opts[OPT_HELP]);
    }
    
    public function testParseArgumentsSetsHelpOptionToFalse()
    {
        $argv = ['program-name'];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertFalse($opts[OPT_HELP]);
    }
    
    public function testParseArgumentsSetsUrl()
    {
        $url = 'http://someurl.com/releases';
        
        // test assignment with = sign
        $argv = ['program-name', '--url='.$url];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals($url, $opts[OPT_URL]);
        
        // test assignment without = sign
        $argv = ['program-name', '--url', $url];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals($url, $opts[OPT_URL]);
    }
    
    public function testParseArgumentsSetsDefaultUrl()
    {
        $argv = ['program-name'];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals(
            'https://download.nextcloud.com/server/releases/',
            $opts[OPT_URL]
        );
    }
    
    public function testParseArgumentsSetsName()
    {
        $name = 'Some name';
        
        // test assignment with = sign
        $argv = ['program-name', '--name='.$name];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals($name, $opts[OPT_NAME]);
        
        // test assignment without = sign
        $argv = ['program-name', '--name', $name];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals($name, $opts[OPT_NAME]);
    }
    
    public function testParseArgumentsSetsDefaultName()
    {
        $argv = ['program-name'];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals(
            'skript.cc/nextcloud-packages',
            $opts[OPT_NAME]
        );
    }
    
    public function testParseArgumentsSetsHomepage()
    {
        $homepage = 'https://nextcloud.skript.cc';
        
        // test assignment with = sign
        $argv = ['program-name', '--homepage='.$homepage];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals($homepage, $opts[OPT_HOMEPAGE]);
        
        // test assignment without = sign
        $argv = ['program-name', '--homepage', $homepage];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals($homepage, $opts[OPT_HOMEPAGE]);
    }
    
    public function testParseArgumentsSetsDefaultHomepage()
    {
        $argv = ['program-name'];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertNull($opts[OPT_HOMEPAGE]);
    }
    
    public function testParseArgumentsSetsApiUrl()
    {
        $apiUrl = '/some/other/uri';
        
        // test assignment with = sign
        $argv = ['program-name', '--api-url='.$apiUrl];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals($apiUrl, $opts[OPT_API_URL]);
        
        // test assignment without = sign
        $argv = ['program-name', '--api-url', $apiUrl];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertEquals($apiUrl, $opts[OPT_API_URL]);
    }
    
    public function testParseArgumentsSetsDefaultApiUrl()
    {
        $apiUrl = '/some/other/uri';
        
        $argv = ['program-name'];
        list($args, $opts) = parseArguments($argv);
        
        $this->assertNull($opts[OPT_API_URL]);
    }
    
    public function testRunOutputsJSON()
    {
        // expect notice from version normalization
        //$this->expectException(Notice::class);
        
        $stdout = fopen('php://memory', 'rw+');
        
        run(
            ['program', '--api-url='.__DIR__.'/Fixtures/api'],
            $stdout
        );
        
        rewind($stdout);
        $output = stream_get_contents($stdout);
        fclose($stdout);
        
        $this->assertJSONStringEqualsJSONFile(
            __DIR__.'/Fixtures/satis.json',
            $output
        );
    }
}